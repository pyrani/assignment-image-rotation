#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "include.h"

enum rotate_status rotateccw(struct image * img)
{
    struct pixel * newdata = malloc( img->height * img->width * sizeof(struct pixel) );
    if ( !newdata ) return STATUS_ERROR_NO_MEMORY;
    uint32_t w,h;
    struct pixel p;
    for ( h = 0; h < img->height; h++ )
        for ( w = 0; w < img->width; w++ )
        {
            p = img->data[ img->width * h + w ];
            newdata[ (img->width - w - 1) * img->height + h ] = p;
        }
    free(img->data);
    img->data = newdata;
    w = img->width;
    img->width = img->height;
    img->height = w;
    return STATUS_OK;
}
