#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "include.h"
struct bmp_header
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
} __attribute__((packed));

struct bmp_header b;

enum read_status from_bmp( FILE* in, struct image * img )
{
    uint32_t n = fread(&b, 1, sizeof(b), in);
    if ( n < sizeof(b) ) return READ_INVALID_HEADER;
    if ( b.bfType != 'B'+('M'<<8) ) return READ_INVALID_SIGNATURE;
    if ( b.biBitCount != 24 ) return READ_INVALID_BITS;

    if ( b.bOffBits != sizeof(b) ) 
        fseek(in, b.bOffBits-sizeof(b), SEEK_CUR);
    uint32_t datasize = b.bfileSize-b.bOffBits;
    uint8_t* bmpdata = malloc(datasize);
    if (!bmpdata) return READ_INVALID_NO_MEMORY;
    n = fread(bmpdata, 1, datasize, in);
    if ( n < datasize ) {
        free(bmpdata);
        return READ_INVALID_FILE_TRUNCATED;
    }
    img->height = b.biHeight;
    img->width = b.biWidth;
    img->data = malloc(b.biWidth * b.biHeight * sizeof(struct pixel));
    if ( !img->data ) {
        free(bmpdata);
        return READ_INVALID_NO_MEMORY;
    }
    uint32_t i,w,h;
    struct pixel p;
    for ( h = 0; h < b.biHeight; h++ )
        for ( w = 0; w < b.biWidth; w++ )
        {
            n = (b.biHeight-h-1) * ( ( b.biWidth * sizeof( struct pixel ) + 3 ) & ~3 );
            p = *(struct pixel*)&bmpdata[ n + w*sizeof( struct pixel )];
            img->data[ h * b.biWidth + w ] = p;
        }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img )
{
    b.bfType = 'B' + ('M' << 8);
    b.bfileSize = sizeof(b)+( ( img->width * sizeof( struct pixel ) + 3 ) & ~3 ) * img->height;
    b.bfReserved = 0;
    b.bOffBits = sizeof(b);
    b.biSize = 40;
    b.biWidth = img->width;
    b.biHeight = img->height;
    b.biPlanes = 1;
    b.biBitCount = 24;
    b.biSizeImage = ( ( img->width * sizeof( struct pixel ) + 3 ) & ~3 ) * img->height;
    b.biCompression = 0;
    b.biXPelsPerMeter = 2834;
    b.biYPelsPerMeter = 2834;
    b.biClrUsed = 0;
    b.biClrImportant = 0;
    fwrite(&b, 1, sizeof(b), out);
    uint32_t zero = 0;
    uint32_t pad = ( ( img->width * sizeof( struct pixel ) + 3 ) & ~3 ) - img->width * sizeof( struct pixel );
    int32_t h;
    uint32_t n;
    for ( h = img->height - 1; h >= 0; h-- )
    {
        n = fwrite(&img->data[ h * img->width ], 1, img->width * sizeof(struct pixel), out);
        if ( n < img->width * sizeof(struct pixel) ) return WRITE_ERROR;
        if (pad) n = fwrite(&zero, 1, pad, out);
        if ( n < pad ) return WRITE_ERROR;
    }
    return WRITE_OK;
}


