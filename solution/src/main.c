#include <stdio.h>
#include "include.h"

void usage() {
  fprintf(stderr,
          "Usage: ./bmprotate BMP_FILE_NAME1 BMP_FILE_NAME2\n");
}


int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        usage();
        return -1;
    }
    FILE* in = fopen( argv[1], "rb" ); 
    struct image img = {0};
    uint8_t ret;
    ret = from_bmp( in, &img );
    if ( ret ) return -1;
    ret = rotateccw( &img );
    if ( ret ) return -1;
    FILE* out = fopen( argv[2], "wb" ); 
    ret = to_bmp( out, &img );
    if ( ret ) return -1;
    fprintf(stdout, "Complete!");
}
