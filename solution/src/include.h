#include  <stdint.h>
struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_FILE_TRUNCATED,
  READ_INVALID_NO_MEMORY
  /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

/*  serializer   */
enum  rotate_status  {
  STATUS_OK = 0,
  STATUS_ERROR_NO_MEMORY
  /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image * img );
enum rotate_status rotateccw(struct image * img);
enum write_status to_bmp( FILE* out, struct image const* img );

